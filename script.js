console.log("ES6 Updates");

/* Exponent Operator */

const SQUARED = Math.pow(4, 3);		//64
console.log(SQUARED);


const SQ = 4 ** 3;
console.log(SQ);



/* Template Literals */
console.log( `Template Literals use Backticks and dollar $ + curly brace {}`);


/* Array Destructuring */

let name = ["Cardo", "Dela Cruz", "Dalisay"];
// console.log(name[0]);
// console.log(name[1]);
// console.log(name[2]);

/*  syntax of array destructuring:

	let [variable, variable, variable] = refArray
*/

	let [fName, mName, lName] = name;
	console.log(fName);
	console.log(mName);
	console.log(lName);


const grades = [91.5, 82.8, 90.9, 85.2];


function herGrades(arrOfgrades){
	// console.log(arrOfgrades)

	const [one, two, , four] = arrOfgrades

	// console.log(one);
	// console.log(two);
	// console.log(four);

	return `Her grade in English is ${one}, in Math ${two} and in Science ${four}`

}

console.log(herGrades(grades));


/* Object Destructuring */

let flower = {
	flowerName: "Rose",
	type: "thornless",
	color: "red"
}

/*  syntax of object destructuring:

	let {property, property, property} = refObject
*/

	// let {flowerName, type, color, season} = flower;
	// console.log(flowerName)
	// console.log(type)
	// console.log(color)
	// console.log(season)

function describeFlower(bulaklak){
	// console.log(bulaklak)

	let {flowerName, type, color} = bulaklak;
	// console.log(flowerName)
	// console.log(type)
	// console.log(color)

	return `I have a ${color} ${type} ${flowerName}.`

}

console.log(describeFlower(flower))



/* Arrow Function */


	/*
	function introduce(){

	}

	*/

const introduce = (name, age) => {
	return `My name is ${name}`
}

console.log(introduce("Joy"))
console.log(introduce("Matthew"))



/* Explicit return of Arrow Function */
console.log(grades)

let mappedGrades = grades.map(grade => {
	return grade
})
console.log(mappedGrades)




/* Implicit return of Arrow Function */

console.log(name);

let mappedName = name.map(person => person && true)
console.log(mappedName);



/* Default Function Argument Value */

const greet = (name = 'guest') => `Good morning, ${name}!`;

console.log(greet())
console.log(greet("Marc"));


function greeting(name = "user"){
	return `Good morning, ${name}!`
}
console.log(greeting())
console.log(greet("Matthew"))



/* Class Based Object blueprints */

	//Syntax of a Class
	class className{
		constructor(propertyA, propertyB){
			this.objProperty = propertyA;
			this.objProperty = propertyB
		}
	}



class Car{
	constructor(name, model, make, color, type){
		this.carName = name;
		this.model = model;
		this.make = make;
		this.color = color;
		this.type = type;
	}
}

let car1 = new Car("Ford", "Fiesta", 2021, "red", "sedan");
console.log(car1)